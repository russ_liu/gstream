package goLinker

import (
	"golang.org/x/exp/constraints"
)

func Sum[T comparable, S constraints.Ordered](targets []T, getterFunc func(T) S) S {
	var result S
	for _, element := range targets {
		result += getterFunc(element)
	}
	return result
}

func Avg[T comparable, S constraints.Integer | constraints.Float](targets []T, getterFunc func(T) S) S {
	return Sum(targets, getterFunc) / S(len(targets))
}

func ToMap[T, S comparable](targets []T, getKeyFunc func(T) S) map[S][]T {
	resultMap := make(map[S][]T)
	for _, element := range targets {
		key := getKeyFunc(element)
		_, ok := resultMap[key]
		if !ok {
			resultMap[key] = make([]T, 0)
		}
		resultMap[key] = append(resultMap[key], element)
	}

	return resultMap
}

func ToSlice[M1 ~map[K]V, V ~[]T, K comparable, T any](target M1) V {
	var result V
	for _, element := range target {
		result = append(result, element...)
	}
	return result
}

func Merge[R ~[]T1, L ~[]T2, T1, T2, M comparable](right R, left L, equalsFunc func(T1, T2) bool, mergeFunc func(T1, T2) *M) []M {
	var result []M
	for _, rightElement := range right {
		for _, leftElement := range left {
			if !equalsFunc(rightElement, leftElement) {
				continue
			}
			result = append(result, *(mergeFunc(rightElement, leftElement)))
		}
	}
	return result
}
